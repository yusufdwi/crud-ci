<?php 
class Biodata extends CI_Controller {
	
	function __construct() {
		parent:: __construct();
	}

	function index() {
		$data['judul']="Biodataku";
		$data['nama']="Moch. Yusuf Dwi Cahyono";
		$data['alamat']="Kediri";
		$data['kelamin']="Laki-laki";
		$data['asal']="SMKTI Kota Kediri";
		$this->load->view('profil',$data);
	}

}

?>