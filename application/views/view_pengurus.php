<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title; ?></title>
</head>
<body>

<!-- untuk tampil data -->
	<h1><?php echo $judul; ?></h1>
	<table border="2">
		<tr>
			<td>NO</td>
			<td>NAMA</td>
			<td>JENIS KELAMIN</td>
			<td>ALAMAT</td>
			<td>GAJI</td>
			<td>AKSI</td>
		</tr>
<?php 
	foreach($data->result_array() as $row) {	
 ?>
		<tr>
			<td><?php echo $row['id']; ?></td>
			<td><?php echo $row['nama']; ?></td>
			<td><?php echo $row['gender']; ?></td>
			<td><?php echo $row['alamat']; ?></td>
			<td><?php echo $row['gaji']; ?></td>
			<td>
				<?php echo anchor('pengurus/edit/'.$row['id'],'Edit'); ?> |
				<?php echo anchor('pengurus/delete/'.$row['id'],'Delete'); ?>
					
			</td>
		</tr>
<?php 
	}
 ?>		
	</table>
<!-- untuk tampil data -->

<!-- untuk tambah data -->
	<?php echo form_open('pengurus/addpengurus'); ?>
	<h1><?php echo $judul1; ?></h1>
	<table>
		<tr>
			<td>ID</td>
			<td><?php echo form_input('id'); ?></td>
		</tr>

		<tr>	
			<td>NAMA</td>
			<td><?php echo form_input('nama'); ?></td>
		</tr>

		<tr>	
			<td>JENIS KELAMIN</td>
			<td><input type="radio" name="gender" value="L">Laki-laki</td>
		</tr>

		<tr>
			<td></td>
			<td><input type="radio" name="gender" value="P">Perempuan</td>
		</tr>

		<tr>	
			<td valign="top">ALAMAT</td>
			<td><textarea name="alamat" rows="3" cols="40"></textarea></td>
		</tr>

		<tr>	
			<td>GAJI</td>
			<td><?php echo form_input('gaji'); ?></td>
		</tr>

		<tr>	
			<td><?php echo form_submit('submit','simpan'); ?> <input type="reset" value="batal"></td>
		</tr>		
	</table>
	<?php echo form_close(); ?>
<!-- untuk tambah data -->

</body>
</html>